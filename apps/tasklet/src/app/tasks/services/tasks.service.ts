import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ITask } from '../../interfaces/task';

@Injectable()
export class TasksService {
  constructor() { }

  saveTask(t: ITask): Observable<void> {

    return;
  }

  deleteTask(id: string): Observable<void> {
    return ;
  }

  getAll(): Observable<ITask[]> {
    return of([]);
  }

  getTask(id: string): Observable<ITask> {
    return of(null);
  }
}
