import { NgModule } from '@angular/core';
import { CoreModule } from '../core/core.module';
import { TasksService } from './services/tasks.service';

/**
 * This module is responsible for managing tasks
 * - creating , editing, deleting tasks
 */

@NgModule({
  declarations: [],
  imports: [
    CoreModule
  ],
  providers: [
    TasksService
  ]
})
export class TasksModule { }
